import 'package:flutter/material.dart';

class DropdownButtonWidget extends StatefulWidget {
  final List<String> lstDropdown;

  const DropdownButtonWidget({super.key, required this.lstDropdown});

  @override
  State<DropdownButtonWidget> createState() => _DropdownButtonWidgetState();
}

class _DropdownButtonWidgetState extends State<DropdownButtonWidget> {
  String? valeurDropdown;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
        value: valeurDropdown,
        elevation: 16,
        isExpanded: true,
        onChanged: (String? valeur) {
          setState(() {
            valeurDropdown = valeur!;
          });
        },
        items:
            widget.lstDropdown.map<DropdownMenuItem<String>>((String valeur) {
          return DropdownMenuItem(value: valeur, child: Text(valeur));
        }).toList());
  }
}
