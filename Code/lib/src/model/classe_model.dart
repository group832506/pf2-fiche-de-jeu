import 'package:fiche_de_jeu/src/model/abstrait/base_donnee.dart';

/// La classe d'un personnage.
class Classe extends BaseDonnee {
  late String id;

  Classe({
    required this.id,
    required String nom,
    String? description,
    Translations? translations,
  }) : super(nom: nom, description: description, translations: translations);

  Classe.fromJson(Map<String, dynamic> json)
      : super(
            nom: json['name'],
            description: json['description'],
            translations: json['translations'] != null
                ? Translations?.fromJson(json['translations'])
                : null) {
    id = json['_id'];
  }

  // A garder pour la sauvegarde plus tard

  /*Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['_id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['translations'] = translations!.toJson();
    return data;
  }*/
}
