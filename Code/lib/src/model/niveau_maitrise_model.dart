enum Maitrises {
  aucune(fr: 'Aucune', en: 'None', lettreFr: '', lettreEn: ''),
  qualifie(fr: 'Qualifié', en: 'Trained', lettreFr: 'Q', lettreEn: 'T'),
  expert(fr: 'Expert', en: 'Expert', lettreFr: 'E', lettreEn: 'E'),
  maitre(fr: 'Maître', en: 'Master', lettreFr: 'M', lettreEn: 'M'),
  legendaire(fr: 'Légendaire', en: 'Legendary', lettreFr: 'L', lettreEn: 'L');

  const Maitrises(
      {required this.fr,
      required this.en,
      required this.lettreFr,
      required this.lettreEn});

  final String fr;
  final String en;
  final String lettreFr;
  final String lettreEn;
}
